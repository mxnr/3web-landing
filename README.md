# Install GRAV

- Clone the Grav repository from [https://github.com/getgrav/grav]() to a folder in the webroot of your server, e.g. `~/webroot/grav`. Launch a **terminal** or **console** and navigate to the webroot folder:
     ```
     $ cd ~/webroot
     $ git clone https://github.com/getgrav/grav.git
     ```
- Install the **plugin** and **theme dependencies** by using the [Grav CLI application](https://learn.getgrav.org/advanced/grav-cli) `bin/grav`:
     ```
     $ cd ~/webroot/grav
     $ bin/grav install
     ```
     
# Run

All you need to do is navigate to the root of your Grav install using the Terminal or Command Prompt and enter ```php -S localhost:8000 system/router.php```. You can replace the port number (in our example it's ```8000```) with any port you prefer.          

# Add Admin panel

```
$ cd ~/webroot/grav
$ bin/gpm install admin
```
     
# Updating

To update Grav you should use the [Grav Package Manager](https://learn.getgrav.org/advanced/grav-gpm) or `GPM`:

```
$ bin/gpm selfupgrade
```

To update plugins and themes:

```
$ bin/gpm update
```          

# Preparation

- Clone this theme to ```~/webroot/grav/user/themes```
- After cloning, change the name of the theme folder from "3web-landing" to "threewebone", this will enable you to see the topic from the Admin panel, 
- If you have the SSH set up, you can do the same using this command ```git clone git@bitbucket.org:mxnr/3web-landing.git threewebone```,
- If SSH is not configured, you can do the same by clicking the clone button and selecting HTTPS, copy the command and add the ```threewebone``` folder name to it and run.

# Installation

- Install dependencies
    ```
    $ cd ~/webroot/grav/user/themes/threewebone
    $ yarn install
    ```
- Compile the styles
    ```
    $ cd ~/webroot/grav/user/themes/threewebone
    $ yarn run build
    ```

# Select the theme

- If you installed the admin panel and registered the user in the admin panel, go to the **themes** tab, select **threewebone** and click save.
- If you do not want to install the admin panel, go to ```~/webroot/grav/user/config/system.yaml``` find default name of the theme **quark** and change to **threewebone**.